var wsUri = "wss://game.mijago.de:2243/";
var debugElem;
var timerId = 0;

function init()
{
    clearInterval(timerId);
	debugElem = document.getElementById("debug");
	HideErrMsg();
	initWebSocket();
	$("form").submit(false);
	document.onkeypress = stopRKey;
}

function initWebSocket() {
	websocket = new WebSocket(wsUri);
	websocket.onopen = function(evt) { onOpen(evt) };
	websocket.onclose = function(evt) { onClose(evt) };
	websocket.onmessage = function(evt) { onMessage(evt) };
	websocket.onerror = function(evt) { onError(evt) };
}

function onOpen(evt) {
	debug("Connection opened");
	send("{\"opcode\":\"init\",\"session\":\""+getCookie("session")+"\",\"name\":\""+getCookie("name")+"\",\"menuid\": \""+getCookie("menuid")+"\"}");	
}

function send(evt) {
	websocket.send(evt);
	debug("send: "+evt);
}

function sendQuiet(evt) {
	websocket.send(evt);
}

function debug(evt) {
	document.getElementById("debug").appendChild(document.createTextNode(evt+"<br>"));
}


function onClose(evt) {
	debug("Connection closed");
	document.getElementById("main").innerHTML = "";
	ShowErrMsg("Verbindung verloren","Leider hast du die Verbindung verloren.<br>Klicke <a href='' onclick='init()'>hier</a> oder lade die Seite neu, um fortzufahren.");
 	timerId = setInterval(function() {
		clearInterval(timerId);
		init();
	},4000);
}

function onMessage(evt) {
	var obj = JSON.parse(evt.data);
	var domsource;
	var domtarget;
	if (obj.ping != null) {
		sendQuiet(evt.data);
		return;
	}
	debug("recv: "+evt.data);
	var method = obj.opcode;
	var source = null;
	if (typeof obj.template != 'undefined') {
		source = document.getElementById(obj.template).innerHTML;
		for (var key in obj.keys) {
            source = source.replace(key,obj.keys[key]);
        }
	}
	if (source == null)
		source = obj.source;
	if (method == "replace")
		document.getElementById(obj.target).innerHTML = source;
	else if (method == "copy") {
		domsource = document.getElementById(source);
		domtarget = document.getElementById(obj.target);
		domtarget.innerHTML = domsource.innerHTML;
	}
	else if (method == "append")
		document.getElementById(obj.target).innerHTML = document.getElementById(obj.target).innerHTML + source;
	else if (method == "remove") {
		var element = document.getElementById(obj.target);
		element.parentNode.removeChild(element);
	}
	else if (method == "prepend")
		document.getElementById(obj.target).innerHTML = source + document.getElementById(obj.target).innerHTML;
	else if (method == "move") {
		domsource = document.getElementById(source);
		domtarget = document.getElementById(obj.target);
		domtarget.appendChild(domsource);
	}
	else if (method == "setcookie")
		setCookie(obj.name, obj.value);
	else if (method == "addclass")
		document.getElementById(obj.target).classList.add(obj.value);
	else if (method == "removeclass")
		document.getElementById(obj.target).classList.remove(obj.value);
}

function onFormSubmit(formId) {
	var x = document.getElementById(formId);
	var json = "{\"opcode\":\"formsubmit\",\"id\":\""+formId+"\"";
	var i;
	var proceed = false;
	for (i = 0; i < x.length; i++) {
		if (x.elements[i].value.length > 0)
			proceed = true;
		json += ",\""+x.elements[i].id+"\":\""+x.elements[i].value.replace(/"/gi,"&quot;")+"\"";
	}
	json += "}";
	if (proceed)
		if (formId === "loginform")
			sendQuiet(json);
		else
			send(json);
	x.reset();
}

function switchMenu(targetId) {
	var json = "{\"opcode\":\"switchmenu\",\"target\":\""+targetId+"\"}";
	send(json);
}

function register() {
	var pass1 = document.getElementById("registerpass");
	var pass2 = document.getElementById("registerpass2");
	var info = document.getElementById("loginstatus");
	info.innerHTML = "Bitte warten...";
	if (pass1.value.length < 8 || pass2.value.length < 8) {
	info.innerHTML = "Passwort zu schwach!";
		return;
	}
	if (pass1.value === pass2.value) {
		onFormSubmit("registerform");
		return;
	}
	
	info.innerHTML = "Passw&ouml;rter nicht gleich!";
}

function stopRKey(evt) {
	var evt = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	var id;
	if ((evt.keyCode == 13) && ((node.type=="text") || node.type=="password"))  {
		id = node.parentNode.id;
		if (id === "registerform")
			return false;
		onFormSubmit(id);
		return false;
	}
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
} 

function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (7*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
} 

function setCssValue(styleName, cssClass, set, attribute, value) {
	if (set) {
		var style = $('<style id="'+styleName+'">'+cssClass+' { '+attribute+': '+value+'; }</style>');
		$('html > head').append(style);
	} else {
		$('#'+styleName).remove();
	}
}

function onError(evt) {
	
	ShowErrMsg("Fehler","Es ist ein Fehler aufgetreten.<br>Klicke <a href='' onclick='HideErrMsg()'>hier</a> um diese Box zu entfernen.");
}

function ShowErrMsg(head,text) {
	$("#errorbox").stop().fadeIn(300);
	$("#errorbox div.errorbox_header").html(head);
	$("#errorbox div.errorbox_text").html(text);
}
function HideErrMsg() {
		$("#errorbox").stop().fadeOut(300);
}

window.addEventListener("load", init, false);
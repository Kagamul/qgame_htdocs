<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	
	<script src="js/jquery.js" language="javascript" type="text/javascript"></script>
	<script src="js/client.js" language="javascript" type="text/javascript"></script>
	<script src="js/design.js" language="javascript" type="text/javascript"></script>
	
	
	<link type="text/css" href="css/jquery.ui.core.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery.ui.theme.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery.ui.selectmenu.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="js/jquery.ui.position.js"></script>
	<script type="text/javascript" src="js/jquery.ui.selectmenu.js"></script>
	
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<title>QGame</title>
	
</head>
<body>
	
<?php
    foreach (scandir("templates") as $key) {
        if ($key == "." or $key == "..") 
            continue;
         echo "<script class='template' type='text' id='template_".preg_replace('/\\.[^.\\s]{3,4}$/', '', $key)."'>".file_get_contents("templates/".$key)."</script>";
    }
?>

<div id="main">Loading...</div>
<script type='text' id="debug"></script>
<div id="errorbox" style="display: none";>
    <div class="errorbox_header">
    	$head$
	</div>
    <div class="errorbox_text">
    $text$
    </div>
</div>
</body>
</html>